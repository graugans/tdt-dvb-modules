# TDT drivers Makefile


CONFIG_KERNEL_PATH  ?= $(KLIB_BUILD)
KERNEL_SRC ?= $(KLIB_BUILD)
KERNEL_LOCATION ?= $(KLIB_BUILD)
CONFIG_KERNEL_BUILD ?= $(KLIB_BUILD)
CONFIG_KERNEL_PATH ?= $(KLIB_BUILD)
CONFIG_MODULES_PATH ?= $(DRIVER_TOPDIR)
CONFIG_STGFB_PATH ?= $(DRIVER_TOPDIR)/stgfb/stmfb-3.1_stm24_0104

CONFIG_PLAYER_191 = y


CCFLAGSY += -D__TDT__ -D__LINUX__ -D__SH4__ -D__KERNEL__ -DMODULE -DEXPORT_SYMTAB
CONFIGFILE := $(DRIVER_TOPDIR)/.config


export CC
export LD

export DRIVER_TOPDIR
export CONFIG_KERNEL_PATH
export KERNEL_SRC
export KERNEL_LOCATION
export CONFIG_KERNEL_BUILD
export CONFIG_KERNEL_PATH
export CONFIG_MODULES_PATH
export CONFIG_STGFB_PATH
export CONFIG_PLAYER_191
export INSTALL_MOD_PATH


include $(CONFIGFILE)

ifdef SPARK
CCFLAGSY+=-DSPARK
endif
ifdef SPARK7162
CCFLAGSY+=-DSPARK7162
endif

EXTRA_CFLAGS = $(CCFLAGSY)

export CCFLAGSY

obj-y := avs/
obj-y += multicom-3.2.4/
obj-y += stgfb/
obj-y += player2_191/
#obj-y += e2_proc/
obj-y += frontends/

obj-y += compcache/
obj-y += bpamem/
obj-y += cec/

ifdef SPARK7162
obj-y += i2c_spi/
endif

modules:
	$(MAKE) ARCH=sh -C $(KLIB_BUILD) CC="${CC}" LD="${LD}"  M=$(DRIVER_TOPDIR) modules
modules_install:
	$(MAKE) ARCH=sh -C $(KLIB_BUILD) CC="${CC}" LD="${LD}"  M=$(DRIVER_TOPDIR) modules_install
clean:
	$(MAKE) ARCH=sh -C $(KLIB_BUILD) M=$(DRIVER_TOPDIR) V=1 clean
